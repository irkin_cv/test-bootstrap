/*global $*/

$(document).ready(function() {
  'use strict';

  $('.jcarousel').jcarousel({});

  $('.jcarousel-control-prev')
    .on('jcarouselcontrol:active', function() {
        $(this).removeClass('inactive');
    })
    .on('jcarouselcontrol:inactive', function() {
        $(this).addClass('inactive');
    })
    .jcarouselControl({
        // Options go here
        target: '-=1'
    });

  // Next control initialization
  $('.jcarousel-control-next')
    .on('jcarouselcontrol:active', function() {
        $(this).removeClass('inactive');
    })
    .on('jcarouselcontrol:inactive', function() {
        $(this).addClass('inactive');
    })
    .jcarouselControl({
        // Options go here
        target: '+=1'
    });
});
